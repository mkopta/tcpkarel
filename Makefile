# Martin 'dum8d0g' Kopta <martin@kopta.eu>
# Mar, 2009
# Makefile

CXX      = g++
#CXXFLAGS = -Wall -Wextra -pedantic -ansi

all: server client

server: tcpkareld.cc
	@# Solaris
	@#$(CXX) $(CXXFLAGS) -ldl -lsocket -o tcpkareld tcpkareld.cc
	@# GNU/Linux
	$(CXX) $(CXXFLAGS) -o tcpkareld tcpkareld.cc

client: tcpkarelc.cc
	@# Solaris
	@#$(CXX) $(CXXFLAGS) -lnsl -lsocket -o tcpkarelc tcpkarelc.cc
	@# GNU/Linux
	$(CXX) $(CXXFLAGS) -o tcpkarelc tcpkarelc.cc

clean:
	rm -f tcpkareld tcpkarelc

# EOF
