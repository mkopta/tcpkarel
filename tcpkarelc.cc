/* Copyright (c) 2009 <Martin Kopta, martin@kopta.eu>

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
****************************************************************/

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <cstring> /* memcpy, strcpy - linux */
#include <cstdlib> /* atoi, abs, .. */

#include <iostream>
#include <sstream>
#include <string>

#define BUFFSIZE 8192

using std::string;
using std::stringstream;

using std::cout;
using std::cerr;
using std::endl;


/* global variables */

int coord_x_old = 0;
int coord_x_new = 0;
int coord_y_old = 0;
int coord_y_new = 0;

string client_cmd;
string robot_name;

char buffer[BUFFSIZE];
int msg_lenght;
int mysocket;
int offset;
struct sockaddr_in xxxsin;
int eof;
int end;

void shutdown(const int retval) {
  close(mysocket);
  exit(retval);
}

void process_incoming() {
  if (buffer[3] != ' ') {
    // in received message must be always at least one space
    cerr << "Server sent unknown message." << endl;
    shutdown(1);
  }

  if (buffer[0] == '2' && buffer[1] == '2' && buffer[2] == '0') {
    /* received message is welcome message */
    string tmp(buffer);
    string key("Oslovuj mne ");  // 12 chars
    int pos1, pos2;
    size_t found;
    found = tmp.find(key);
    if (found != string::npos)
      pos1 = static_cast<int>(found);
    pos2 = tmp.length();
    pos2--;
    pos2--;
    tmp = string(tmp, pos1+12, pos2);
    char tmp2[4096];
    strcpy(tmp2 , tmp.c_str());
    int i = -1;
    char x;
    do {
      i++;
      x = tmp2[i];
    } while (x != '\r' && x != '\n' && x != '\0' && x != '.');
    robot_name = string(tmp2, 0, i);
    cout << "Robots name is: " << robot_name << endl;
    if (robot_name[0] == ' ' || robot_name[robot_name.length()-1] == ' ') {
      cerr << "Server give invalid name of robot" << endl;
      shutdown(1);
    }
  } else if (buffer[0] == '2' && buffer[1] == '2' && buffer[2] == '1') {
    /* recieved message is a text of the sign */
    cout << "Text at the sign: " << endl;
    cout << "\t";
    for (int x = 11; x < eof; x ++) {
      cout << buffer[x];
    }
    cout << endl;
    shutdown(0);
  } else if (buffer[0] == '2' && buffer[1] == '5' && buffer[2] == '0') {
    /* Operation was succesful, new coordinates found */
    char c;
    char xxx[3] = { '\0', '\0', '\0' };
    char yyy[3] = { '\0', '\0', '\0' };
    int left = 0;
    int right = 0;
    int comma = 0;

    coord_x_old = coord_x_new;
    coord_y_old = coord_y_new;

    /* searching for coordinates */
    for (int g = 4; g < BUFFSIZE; g++) {
      c = buffer[g];
      if (c == '(') left = g;
      if (c == ')') right = g;
      if (c == ',') comma = g;
      if (c == '\n' || c == '\r' || c == '\0')
        break;   // end of msg
      if (left > 0 && right > 0 && comma > 0)
        break;   // all found
    }
    if (!(left > 0 && right > 0 && comma > 0)) {
      cerr << "Message with new coordinates is badly formated." << endl;
    }

    /* parsing the coordinates */
    int z = 0;

    for (int g = left+1; g < comma; g++) {
      xxx[z] = buffer[g];
      z++;
    }

    z = 0;

    for (int g = comma+1; g < right; g++) {
      yyy[z] = buffer[g];
      z++;
    }
    coord_x_new = atoi(xxx);
    coord_y_new = atoi(yyy);
  } else if (buffer[0] == '5' && buffer[1] == '0' && buffer[2] == '0') {
    /* unknown response */
    cout << "Server sent unknown message. Exiting." << endl;
    shutdown(0);
  } else if (buffer[0] == '5' && buffer[1] == '3' && buffer[2] == '0') {
    /* crash */
    cout << "Robot crashed. Mission failed. Sorry." << endl;
    shutdown(0);
  } else if (buffer[0] == '5' && buffer[1] == '5' && buffer[2] == '0') {
    /* reading message from sign failed */
    cout << "Mission failed. Cannot read sign." << endl;
    shutdown(0);
  }
}

void send_message() {
  string message_to_send;
  stringstream ss;
  ss << robot_name;
  ss << " ";
  ss << client_cmd;
  ss << "\r\n";
  message_to_send = ss.str();

  cout << "Outgoing message: " << message_to_send;
  /* sending message */
  if (write(mysocket, message_to_send.c_str(), (signed int)
        message_to_send.length()) != (signed int)message_to_send.length()) {
    cerr << "Sending message to server failed." << endl;
    shutdown(1);
  }

  /* receive the answer */
  offset = 0;
  end = 0;
  int i;
  do {
    msg_lenght = read(mysocket, buffer + offset, BUFFSIZE - offset);
    if (msg_lenght == -1) {
      cerr << "Receiving message failed." << endl;
      shutdown(1);
    }
    offset += msg_lenght;
    for (i = 0; i < BUFFSIZE -1; i ++) {
      if (buffer[i] == '\r' && buffer[i+1] == '\n') {
        end = 1;
        break;
      }
    }
  }
  while (offset != BUFFSIZE && !end);
  if (!end) {
    cerr << "Incoming message is badly formated." << endl;
    shutdown(1);
  }
  buffer[i] = '\0';
  eof = i;

  cout << "Incoming message: '" << buffer << "'" << endl;

  process_incoming();
}

void robot_read_sign() {
  client_cmd = "ZVEDNI";
  send_message();
}

void robot_test() {
  if (coord_x_new == 0 && coord_y_new == 0)
    robot_read_sign();
}

void robot_turn_left() {
  client_cmd = "VLEVO";
  send_message();
}

void robot_step() {
  client_cmd = "KROK";
  send_message();
}

void guide_robot_to_mark() {
  robot_turn_left();
  robot_test();
  robot_step();
  robot_test();

  /* if we are closer to sign, go ahead the same direction */
  if (abs(coord_x_new ) < abs(coord_x_old) ||
      abs(coord_y_new) < abs(coord_y_old)) {
    bool go = true;

    if (abs(coord_x_new) < abs(coord_x_old) && coord_x_new == 0)
      go = false;
    if (abs(coord_y_new) < abs(coord_y_old) && coord_y_new == 0)
      go = false;

    while (go) {
      robot_step();
      if (coord_x_new != coord_x_old && coord_x_new == 0)
        break;
      if (coord_y_new != coord_y_old && coord_y_new == 0)
        break;
    }
  } else {
    /* turn robot to oposite direction */
    robot_turn_left();
    robot_turn_left();
    bool go = true;

    if (abs(coord_x_new) < abs(coord_x_old) && coord_x_new == 0)
    { go = false; }
    if (abs(coord_y_new) < abs(coord_y_old) && coord_y_new == 0)
    { go = false; }

    while (go) {
      /* stepping to sign */
      robot_step();
      if (coord_x_new != coord_x_old && coord_x_new == 0)break;
      if (coord_y_new != coord_y_old && coord_y_new == 0)break;
    }
  }

  robot_test();
  /* now we try to null second coordinate */
  robot_turn_left();
  robot_step();
  robot_test();
  if (abs(coord_x_new ) < abs(coord_x_old) ||
      abs(coord_y_new) < abs(coord_y_old)) {
    /* robot has sign in front of his nose */
  } else {
    robot_turn_left();
    robot_turn_left();
  }
  while (1) {
    robot_step();
    if (coord_x_new == 0 && coord_y_new == 0)
      break;
  }
  /* now we should be at the sign */
  robot_read_sign();
  return;
}


int main(const int argc, char** argv) {
  /* args check */
  if (argc != 3) {
    cerr << "Usage: " << *argv << " <hostname> <port>" << endl;
    return 1;
  }

  /* args processing */
  argv++;
  char * hostname = *argv;
  argv++;
  int port = atoi(*argv);
  if (port < 1 || port > 65535) {
    cerr << "Port has to be in range <1, 65535> !" << endl;
    return 1;
  }

  cout << "Connecting to '" << hostname;
  cout << "' on port '" << port << "'." << endl;

  /* creating socket */
  mysocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
  if (mysocket == -1) {
    cerr << "Cannot create socket" << endl;
    return 1;
  }

  /* connection parameters */
  memset(&xxxsin, 0, sizeof(xxxsin));
  xxxsin.sin_family = AF_INET;
  xxxsin.sin_port = htons(port);
  xxxsin.sin_addr.s_addr = inet_addr(hostname);
  if (connect(mysocket, (struct sockaddr *)& xxxsin, sizeof(xxxsin))== - 1) {
    cerr << "Nelze se pripojit na server." << endl;
    shutdown(1);
  }

  /* recieve the welcome message */
  offset = 0;
  end = 0;
  int i;
  do {
    msg_lenght = read(mysocket, buffer + offset, BUFFSIZE - offset);
    if (msg_lenght == -1) {
      cerr << "Socket read error." << endl;
      shutdown(1);
    }
    offset += msg_lenght;

    for (i = 0; i < BUFFSIZE -1; i ++) {
      if (buffer[i] == '\r' && buffer[i+1] == '\n') {
        end = 1;
        break;
      }
    }
  } while (offset != BUFFSIZE && !end);
  if (!end) {
    cerr << "Incoming message is badly formated." << endl;
    shutdown(1);
  }

  buffer[i] = '\0';
  eof = i;
  process_incoming();
  guide_robot_to_mark();

  close(mysocket);
  return 0;
}

/* EOF */
