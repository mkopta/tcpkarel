/* Copyright (c) 2009 <Martin Kopta, martin@kopta.eu>

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
****************************************************************/

#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <time.h>
#include <unistd.h>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <sstream>

using std::cout;
using std::endl;
using std::string;
using std::stringstream;
using std::cerr;

const int BUFFSIZE = 2097152;
const char *names[10] =
{ "Christopher Columbus", "Bartolomeu Dias", "Magellan", "Henry Hudson",
"J Cook", "Amundsen", "Neil", /* Armstrong;-)*/ "Frank Cole",
"Marco Polo", "Captain Kirk" };
const char *s_messages[50] =
{ "Chuck Norris napocital do nekonecna. Dvakrat.",
"Chuck Norris zemrel pred 10 lety ale Smrtka nemela odvahu mu to rict.",
"Chuck Norris umi delit nulou.",
"Chuck Norris musi tridit pradlo na tri hromady - bile, barevne a krvave.",
"Kdyz jde Bubak spat, kazdou noc se podiva pod postel jestli tam neni Chuck Norris.",
"Chuck Norris je castym darcem krve pro Cerveny kriz. Jen ne svoji vlastni.",
"Chuck Norris hral ruskou ruletu s plne nabitou zbrani a vyhral.",
"Kdyz Chuck Norris spadne do vody, nebude mokry, ale voda bude Chuckonorrisova.",
"Teorie evoluce neexistuje, existuje pouze seznam zivocisnych druhu, ktere nechal Chuck Norris prezit.",
"Pokud ma Chuck Norris zpozdeni, cas by mel radsi sakra zpomalit.",
"Chuck Norris dokaze palit mravence lupou. V noci.",
"Chuck Norris je prvni clovek, ktery porazil betonovou zed v tenise.",
"Slzy Chucka Norrise leci rakovinu. Skoda ze nikdy neplakal.",
"Na posledni strance Guinnessovy knihy svetovych rekordu je malym pismem uvedeno ze vsechny rekordy drzi Chuck Norris, zde uvedeni lide jsou ti co jsou mu nejbliz",
"Nekteri lide nosi pyjamo se Supermanem. Superman nosi pyjamo s Chuckem Norrisem.",
"Nejsou zadne odlisne rasy, jenom zeme plne lidi, ktere Chuck Norris zbil do ruznych odstinu cerne, modre a zlute.",
"Chuck Norris si v KFC objednal BigMac a dostal ho.",
"V Iraku nebyly zadne zbrane hromadneho niceni. Chuck Norris zije v Oklahome.",
"Chuck Norris znicil periodickou tabulku prvku protoze uznava pouze prvek prekvapeni.",
"Na Nagasaki nikdy zadna bomba nespadla, to jen Chuck Norris vyskocil z letadla a uderil do zeme.",
"V prumernem obyvacim pokoji je 1242 veci, pomoci nichz by Vas Chuck Norris dokazal zabit Vcetne samotneho pokoje.",
"Chuck Norris nespi, jen ceka.",
"Zbrane nezabijeji, to Chuck Norris.",
"Hlavni priciny smrti v USA jsou: 1.Onemocneni srdce, 2.Chuck Norris, 3.Rakovina.",
"Okolni vesmir existuje, protoze se boji byt na stejne planete jako Chuck Norris.",
"Chuck Norris necte knihy, jen je poradne zmackne a dostane informace, co potrebuje.",
"Chuck Norris je tak rychly, ze muze obehnout Zemi a uderit se do zad.",
"Chuck Norris nenosi hodinky, on proste urci kolik je hodin.",
"Vetsina lidi ma 23 paru chromozomu Chuck jich ma 72 a vsechny jsou smrtelne.",
"Chuck Norris zna posledni cifru cisla Pi.",
"Nekteri lide maji jednu mozkovou hemisferu vetsi nez druhou Chuck Norris ma vetsi obe.",
"Chuck Norris vynalezl cele spektrum barev krome ruzove. Tu vynalezl Tom Cruise.",
"Chuck Norris umi roztahnout diamant a promenit ho tak v uhli.",
"zadne globalni oteplovani neexistuje, to jen Chuck Norris priblizil Slunce, kdyz mu bylo chladno.",
"Za kazdym uspesnym muzem je zena, za kazdym mrtvym muzem je Chuck Norris.",
"Kdyz buh rekl 'budiz svetlo', Chuck Norris cekal az poprosi.",
"Chuck Norris jednou kopl kone do brady. Jeho potomci jsou dnes znami jako zirafy.",
"Ozzy Osbourne ukusuje hlavy netopyrum, Chuck Norris to dela tygrum usurijskym.",
"Chuck Norris umi delat bubliny z hovezich steaku.",
"Kdyz se rika 'nikdo neni dokonaly' Chuck Norris to povazuje za osobni urazku.",
"Kdyz Chuck Norris dela drepy, nezveda se nahoru, ale tlaci Zemi dolu.",
"Chuck Norris dokaze donutit vodu tect do kopce.",
"Kdyz Chuck Norris prechazi ulici, vsechna auta se musi rozhlednout.",
"Jednou Chuck Norris zahodil bumerang a ten se uz nikdy nevratil, protoze se Chucka boji.",
"Chuck Norris umi tleskat jednou rukou.",
"Chuck Norris si striha vlasy motorovou pilou.",
"Kdyz ziskate ve hre Scrabble jmeno Chuck Norris tak jste vyhrali. Navzdy.",
"V bibli dokazal jezis premenit vodu na vino. Chuck Norris ho pak zmenil na pivo.",
"Chuck Norris je primo za tebou.",
"If Chuck Norris says false, it's always true." };

class Game {
  private:
    int    sockfd;
    int    client_num;
    string outgoing;
    char   incoming[BUFFSIZE];

    int    coord_x;
    int    coord_y;
    int    rotation;
    string name_of_robot;
    string cmd_KROK;
    string cmd_VLEVO;
    string cmd_ZVEDNI;
    string text_on_the_sign;
    bool   game_over;

    int random_number(int min, int max) {
      return rand() % (max - min + 1) + min;
    }

    string msg_221(void) {
      stringstream ss;
      ss << "221 USPECH " << this->text_on_the_sign << "\r\n";
      return ss.str();
    }

    string msg_250(void) {
      stringstream ss;
      ss << "250 OK(" << this->coord_x;
      ss << "," << this->coord_y << ")\r\n";
      return ss.str();
    }

    string msg_500(void) {
      return "500 NEZNAMY PRIKAZ\r\n";
    }

    string msg_530(void) {
      return "530 HAVARIE\r\n";
    }

    string msg_550(void) {
      return "550 NELZE ZVEDNOUT ZNACKU\r\n";
    }

    void step_forward(void) {
      if (this->rotation == 0)this->coord_y++;
      if (this->rotation == 1)this->coord_x++;
      if (this->rotation == 2)this->coord_y--;
      if (this->rotation == 3)this->coord_x--;
    }

    void turn_left() {
      this->rotation--;
      if (this->rotation == -1)this->rotation = 3;
    }

    bool out_of_map(void) {
      if (this->coord_x > 15 || this->coord_x < -15 ||
          this->coord_y > 15 || this->coord_y < -15) {
        return true;
      } else {
        return false;
      }
    }

    bool robot_is_on_mark(void) {
      if (this->coord_x == 0 && this->coord_y == 0) {
        return true;
      } else {
        return false;
      }
    }

    void print_game_state(void) {
      cout << "[" << this->client_num << "] Robot name is '";
      cout << this->name_of_robot << "', it's at coords <";
      cout << this->coord_x << ";" << this->coord_y << "> and it's looking ";
      if (this->rotation == 0)cout << "up." << endl;
      if (this->rotation == 1)cout << "right." << endl;
      if (this->rotation == 2)cout << "down." << endl;
      if (this->rotation == 3)cout << "left." << endl;
    }

    void welcome_message(void) {
      int retval;
      int num = random_number(0, 3);
      string msg;
      stringstream ss;
      ss << "220 ";
      switch (num) {
        case 0:
          ss << "Objevitelsky robot pripraven. Oslovuj mne ";
          ss << this->name_of_robot << ".";
          ss << " Cekam na prikazy.";
          break;
        case 1:
          ss << "AHOJ, JA JSEM TVUJ ROBOT. Oslovuj mne ";
          ss << this->name_of_robot << ".";
          break;
        case 2:
          ss << "Oslovuj mne " << this->name_of_robot;
          break;
        case 3:
          ss << "READY. Oslovuj mne " << this->name_of_robot;
          break;
      }
      ss << "\r\n";
      msg = ss.str();
      cout << "[" << this->client_num << "] Outgoing message: " << msg;
      retval = send(this->sockfd, msg.c_str(), msg.size(), 0);
      if (retval == -1) {
        cerr << "[" << this->client_num;
        cerr << "] Error during 'send' action.." << endl;
        shutdown(this->sockfd, SHUT_RDWR);
        close(this->sockfd);
        exit(EXIT_FAILURE);
      }
    }

    int get_next_command(void) {
      int msg_length;
      int offset = 0;
      int end    = 0;
      int i;

      memset(&incoming, '\0', sizeof(incoming));

      do {
        msg_length = recv(this->sockfd, incoming + offset,
            BUFFSIZE - offset, 0);
        if (msg_length == -1) {
          cerr << "[" << this->client_num;
          cerr << "] Error during 'recv' action.." << endl;
          shutdown(this->sockfd, SHUT_RDWR);
          close(this->sockfd);
          exit(EXIT_FAILURE);
        }
        offset += msg_length;
        for (i = 0; i < BUFFSIZE - 1; i ++) {
          if (incoming[i] == '\r' && incoming[i+1] == '\n') {
            end = 1;
            break;
          }
        }
      } while (offset != BUFFSIZE && !end);
      if (!end) {
        cerr << "[" << this->client_num << "] Incomming message ";
        cerr << "is badly formated." << endl;
        shutdown(this->sockfd, SHUT_RDWR);
        close(this->sockfd);
        exit(EXIT_FAILURE);
      }
      incoming[i] = '\0';
      return i;
    }

    void process_command(string cmd) {
      cout << "[" << this->client_num << "] Incoming message: ";
      cout << cmd << endl;
      if (cmd == this->cmd_KROK) {
        step_forward();
        if (out_of_map()) {
          this->outgoing = msg_530();
          this->game_over = true;
        } else {
          this->outgoing = msg_250();
        }
      } else if (cmd == this->cmd_VLEVO) {
        turn_left();
        this->outgoing = msg_250();
      } else if (cmd == this->cmd_ZVEDNI) {
        if (robot_is_on_mark()) {
          this->outgoing = msg_221();
        } else {
          this->outgoing = msg_550();
        }
        this->game_over = true;
      } else {
        this->outgoing = msg_500();
      }
      return;
    }

    void respond(void) {
      int retval;
      cout << "[" << this->client_num << "] Outgoing message: ";
      cout << this->outgoing;
      retval = send(this->sockfd, this->outgoing.c_str(),
          this->outgoing.size(), 0);
      if (retval == -1) {
        cerr << "[" << this->client_num << "] Error during 'send' action." << endl;
        shutdown(this->sockfd, SHUT_RDWR);
        close(this->sockfd);
        exit(EXIT_FAILURE);
      }
      return;
    }

  public:
    Game(const int _client_num, const int c_sockfd) {
      this->client_num = _client_num;
      this->sockfd = c_sockfd;
      this->game_over = false;
      srand((unsigned)time(NULL));
      this->name_of_robot = string(names[random_number(0, 9)]);
      this->coord_x = random_number(-14, +14);
      this->coord_y = random_number(-14, +14);
      this->rotation = random_number(0, 3);
      this->text_on_the_sign = string(s_messages[random_number(0, 49)]);
      this->cmd_KROK = this->name_of_robot + string(" KROK");
      this->cmd_VLEVO = this->name_of_robot + string(" VLEVO");
      this->cmd_ZVEDNI = this->name_of_robot + string(" ZVEDNI");
    }

    ~Game(void) {
      shutdown(this->sockfd, SHUT_RDWR);
      close(this->sockfd);
    }

    void run(void) {
      string cmd;
      print_game_state();
      this->welcome_message();
      int retval;
      do {
        retval = get_next_command();
        process_command(string(this->incoming, 0, retval));
        respond();
      }
      while (!game_over);
      cout << "[" << this->client_num << "] QUIT" << endl;
      shutdown(this->sockfd, SHUT_RDWR);
      close(this->sockfd);
      return;
    }
};

class Splitter {
  private:
    int port;
    int sockfd;
    struct sockaddr_in address;
    char incomming[BUFFSIZE];
    int client_num;

    void new_connection_handler(int c_sockfd) {
      Game game(this->client_num, c_sockfd);
      game.run();
    }

  public:
    explicit Splitter(int _port) {
      this->port = _port;
      this->client_num = 0;
    }

    ~Splitter(void) {
      close(this->sockfd);
    }

    int init(void) {
      int retval = 0;
      /* Create socket */
      cout << " * Creating main socket.. " << endl;
      this->sockfd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
      if (this->sockfd == -1) {
        cerr << " * Can not create new socket." << endl;
        return 1;
      }
      /* Bind socket */
      cout << " * Binding main socket.. " << endl;
      memset(&(this->address), 0, sizeof(this->address));
      this->address.sin_family = AF_INET;
      this->address.sin_port   = htons(this->port);
      retval = bind(sockfd, (struct sockaddr *)&(this->address),
          sizeof(this->address));
      if (retval == -1) {
        cerr << " * Error during 'bind' action." << endl;
        return 1;
      }
      /* Listen on socket */
      cout << " * Listening on main socket.. " << endl;
      if (listen(this->sockfd, 5)== -1) {
        cerr << " * Error during 'listen' action." << endl;
        return 1;
      }
      /* Init done successfuly */
      cout << " * === Server ready ===" << endl;
      return 0;
    }

    void loop(void) {
      pid_t pid;
      int c_sockfd;
      struct sockaddr_in rem_addr;
      /* sometimes unsigned, sometimes not */
      unsigned int rem_addr_length;

      /* Wait for connections from clients */
      while (true) {
        cout << " * Ready for client connection" << endl;
        rem_addr_length = sizeof(rem_addr);
        c_sockfd = accept(this->sockfd,
           (struct sockaddr *)&rem_addr, &rem_addr_length);
        if (c_sockfd == -1) {
          cerr << " * Error during 'accept' action." << endl;
          continue;
        } else {
          cout << " * Incomming connection" << endl;
        }
        this->client_num++;
        pid = fork();
        if (pid > 0) {  // parent
          cout << " * Client number " << this->client_num;
          cout << " is served by process with pid ";
          cout << static_cast<int>(pid) << endl;
        } else if (pid == 0) {  // child
          new_connection_handler(c_sockfd);
          exit(EXIT_SUCCESS);
        } else if (pid < 0) {  // error
          cout << " * fork() failed!" << endl;
          continue;
        }
      }
    }
};

int main(int argc, char **argv) {
  int port;
  int retval;

  cout << "kareld-d8g v1.0" << endl;
  cout << "(c) 2009 Martin 'dum8d0g' Kopta <martin@kopta.eu>" << endl;
  cout << "Licensed under MIT license" << endl << endl;

  /* Command-line arguments processing */
  if (argc < 2) {
    cout << "Usage: " << *argv << " <port>" << endl;
    return EXIT_FAILURE;
  } else {
    ++argv;
    port = atoi(*argv);
    if (port < 0 || port > 65535) {
      cout << " * Given port is out of range(0;65535)!" << endl;
      return EXIT_FAILURE;
    }
  }

  /* Start server */
  Splitter kareld(port);
  retval = kareld.init();
  if (retval != 0) {
    cerr << " * Starting server failed." << endl;
    return EXIT_FAILURE;
  }
  kareld.loop();

  return EXIT_SUCCESS;
}

/* EOF */
